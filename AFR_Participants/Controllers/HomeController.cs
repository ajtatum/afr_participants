﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AFR_Participants.Models;

namespace AFR_Participants.Controllers
{
    public class HomeController : Controller
    {
        private afr_bike_rideEntities _db = new afr_bike_rideEntities();

        public ActionResult Index(string command)
        {
            Participant nextWinner = new Participant();
            if (!String.IsNullOrEmpty(command))
            {
                if (command.Equals("Get Next Winner"))
                {
                    var items = _db.Participants.Where(i => i.WonPrize == false);
                    if ((items != null) && (items.Count() > 0))
                    {
                        Random rand = new Random(DateTime.Now.Millisecond);
                        int pick = rand.Next(0, items.Count() - 1);
                        int current = 0;
                        foreach (var item in items)
                        {
                            if (current == pick)
                            {
                                item.WonPrize = true;
                                nextWinner = item;
                                break;
                            }
                            current++;
                        }
                        _db.SaveChanges();
                    }
                }
                else if (command.Equals("Clear Rider Won List"))
                {
                    foreach (var item in _db.Participants)
                    {
                        item.WonPrize = false;
                    }
                    _db.SaveChanges();
                }
            }

            var riderWinner = new RiderWinnerViewModel();
            riderWinner.Participants = _db.Participants.ToList();
            riderWinner.NextWinner = nextWinner;

            return View(riderWinner);
        }

        public ActionResult AddRider()
        {
            RiderViewModel model = new RiderViewModel();
            model.BibNumber = string.Empty;
            model.FirstName = string.Empty;
            model.LastName = string.Empty;
            return View(model);
        }

        [HttpPost]
        public ActionResult AddRider(RiderViewModel model)
        {
            var participants = _db.Participants;
            participants.Add(new Participant()
            {
                BibNumber = model.BibNumber,
                FirstName = model.FirstName,
                LastName = model.LastName,
                WonPrize = false
            });
            _db.SaveChanges();

            model.BibNumber = string.Empty;
            model.FirstName = string.Empty;
            model.LastName = string.Empty;
            ModelState.Clear();

            return View(model);
        }
    }
}