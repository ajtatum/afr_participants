﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AFR_Participants.Models
{
    public class RiderWinnerViewModel
    {
        public List<Participant> Participants { get; set; }
        public Participant NextWinner { get; set; }
    }
}