﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AFR_Participants.Models
{
    public class RiderViewModel
    {
        public string BibNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}