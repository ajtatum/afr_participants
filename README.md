# README #

### What is this repository for? ###

AFR Participants is a simple, "quick & dirty" application written for my non-profit, Active For Recovery. The application was written to do one simple job: to select a random winner out of a list of participants and "cross out" that winner so they couldn't win another prize.